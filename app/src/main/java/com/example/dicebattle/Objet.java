package com.example.dicebattle;

/**
 * Classe Mère de tous les objets
 */
public class Objet {

    private String nomObjet;
    private int effetVie;
    private String description;
    private int imageObjet;
    private int mana;

    /**
     * Ceci est le constructeur de base de la classe Objet
     * @param nom est le nom de l objet
     * @param descript est la description de l objet
     * @param view est l'image de l objet
     * @param mana1 est la mana que prend l objet
     */
    Objet(String nom,String descript,int view,int mana1)
    {
        setNomObjet(nom);
        setDescription(descript);
        setImageObjet(view);
        setMana(mana1);
    }

    /**
     * Applique l'effet de l'objet sur le joueur
     * @return
     */
    public Joueur effetObjet()
    {

        Joueur joueur = null;
        return joueur;

    }

    public void ConsummerMana()
    {



    }

    /**
     * Ceci est le getter du nom de l objet
     * @return
     */
    public String getNomObjet() {
        return nomObjet;
    }

    /**
     * Ceci est le setter du nom de l objet
     * @param nomObjet
     */
    public void setNomObjet(String nomObjet) {
        this.nomObjet = nomObjet;
    }

    /**
     * Ceci est le getter de effetVie
     * @return
     */
    public int getEffetVie() {
        return effetVie;
    }

    /**
     * Ceci est le setter de effetVie
     * @param effetVie
     */
    public void setEffetVie(int effetVie) {
        this.effetVie = effetVie;
    }

    /**
     * Ceci est le getter de description
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Ceci est le setter de description
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Ceci est le getter de ImageObjet
     * @return
     */
    public int getImageObjet() {
        return imageObjet;
    }

    /**
     * Ceci est le setter de imageObjet
     * @param imageObjet
     */
    public void setImageObjet(int imageObjet) {
        this.imageObjet = imageObjet;
    }

    /**
     * Ceci est le getter de mana
     * @return
     */
    public int getMana() {
        return mana;
    }

    /**
     * Ceci est le setter de mana
     * @param mana
     */
    public void setMana(int mana) {
        this.mana = mana;
    }
}
