package com.example.dicebattle;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static com.example.dicebattle.PageAccueil.mydb;

/**
 * Page lorsqu'un joueur gagne
 */
public class PageGameOver extends Activity {

    private Joueur joueurGagant;
    private int vieJoueur1;
    private int vieJoueur2;
    private  TextView textjoueurGagaant;
    private TextView textvieJoueur1;
    private TextView textvieJoueur2;
    private TextView textViewVictoire;
    private String nomJoueur1;
    private String nomJoueur2;
    private Button boutonChangerNom;
    private Button buttonRecommencer;
    private MediaPlayer player = null;


    /**
     * Constructeur de la PageGameOver
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gameover);

        //Crie Victoire
        if(player == null)player = MediaPlayer.create(this,R.raw.victory);
        player.start();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                player.release();
                player = null;
            }
        });

        //intent
        Intent intent = getIntent();
        joueurGagant = intent.getParcelableExtra("NomJoueurGagnant");
        vieJoueur1 = intent.getIntExtra("vieJoueur1",0);
        vieJoueur2 = intent.getIntExtra("vieJoueur2",0);
        nomJoueur1 = intent.getStringExtra("nomJoueur1");
        nomJoueur2 = intent.getStringExtra("nomJoueur2");



        textjoueurGagaant = (TextView)findViewById(R.id.joueurGagné);
        textvieJoueur1 = (TextView)findViewById(R.id.gameOverVieJoueur1);
        textvieJoueur2 = (TextView)findViewById(R.id.gameOverVieJoueur2);

        textViewVictoire = (TextView)findViewById(R.id.textViewNbVictoire);
        textjoueurGagaant.setText(joueurGagant.getNom() +" "+ getString(R.string.a_gagné));
        textvieJoueur1.setText(String.valueOf(vieJoueur1)+"/40");
        textvieJoueur2.setText(String.valueOf(vieJoueur2)+"/40");

        //gérer les victoires de la base de données
        Cursor resultSet;
        int nbVictoire;

        resultSet = mydb.rawQuery("Select * from Joueur WHERE nom = ?", new String[]{joueurGagant.getNom()});
        resultSet.moveToFirst();
        nbVictoire = resultSet.getInt(1) + 1;

        mydb.execSQL("UPDATE Joueur SET victoire  = '"+nbVictoire+"' WHERE nom = ?", new String[]{joueurGagant.getNom()});
        textViewVictoire.setText("Nombre de victoires : "+ String.valueOf(nbVictoire));



        //couleur du textView du nom du gagant
        if(vieJoueur1 == 0)
        {
            textjoueurGagaant.setTextColor(getResources().getColor(R.color.rouge));

        }
        else {

            textjoueurGagaant.setTextColor(getResources().getColor(R.color.bleu));

        }


        buttonRecommencer = (Button)findViewById(R.id.buttonRecommencer);

        buttonRecommencer.setOnClickListener(new View.OnClickListener() {
            /**
             * Reset et Retour au MainACtivity
             * @param v
             */
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(PageGameOver.this,MainActivity.class);
                intent1.putExtra("nomJoueur1",nomJoueur1);
                intent1.putExtra("nomJoueur2",nomJoueur2);
                startActivity(intent1);
                finish();
            }
        });


        boutonChangerNom = findViewById(R.id.buttonChangeName);


        boutonChangerNom.setOnClickListener(new View.OnClickListener() {
            /**
             * Retour a Page Accueil
             * @param v
             */
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(PageGameOver.this,PageAccueil.class);
                startActivity(intent2);
                finish();
            }
        });
    }












}
