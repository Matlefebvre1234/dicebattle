package com.example.dicebattle;

/**
 * Objet grossePotion redonne 8 points de vie
 */
public class GrossePotion  extends Objet {
    Joueur joueur;

    /**
     * Ceci est le constructeur de base de la classe GrossePotion est il herite de la classe Joueur
     * @param player herite de la classe Joueur
     */
    GrossePotion(Joueur player) {
        super("Grosse potion", " +8 points de vie", R.drawable.grossepotion1,3);

        joueur = player;

    }


    @Override

    /**
     * La fonction effetObjet enleve de la vie au joueur ainsi que de la mana
     * @return joueur
     */
    public Joueur effetObjet() {

        joueur.setVie(joueur.getVie() + 8);
        joueur.setMana(joueur.getMana() - 3);

        return joueur;
    }

    @Override
    public void ConsummerMana() {

    }

}
