package com.example.dicebattle;

import android.widget.ImageView;

import static com.example.dicebattle.MainActivity.RANDOM;

/**
 * Dé avec 6 faces
 */
public class Dé {

    private int imageDe;
    private int valeurDé;


    /**
     * Ceci est le constructeur de base de la classe Dé
     */
    Dé()
    {
       imageDe = 0;
       valeurDé = 0;
    }




    /**
     * Ceci est le deuxieme constructeur qui prend en paramètre un int(valDé)
     * @param valDé
     */
    Dé(int valDé)
    {
        setValeurDé(valDé);
        setImageDe(R.drawable.d1);
    }



    /**
     * Cette fonction genere un nombre aléatoire pour un dé et retourne la valeur
     * @return retourne la valeur du dé
     */
    public int randomDiceValue() {
        int valeur = RANDOM.nextInt(6) + 1;
        valeurDé = valeur;
        return valeur;
    }




    /**
     * Ceci est le getter de ImageDe et il retourne imageDe
     * @return retourne la valeur en int de imageDe
     */
    public int getImageDe() {
        return imageDe;
    }




    /**
     * Ceci est le setter de imageDe et il prend en paramètre la valeur en int de imageDe
     * @param imageDe
     */
    public void setImageDe(int imageDe) {
        this.imageDe = imageDe;

    }



    /**
     * Ceci est le getter de ValeurDe et il retourne valeurDe
     * @return retourne la valeur en int de valeurDe
     */
    public int getValeurDé() {
        return valeurDé;
    }



    /**
     * Ceci est le setter de valeurDe et il prend en paramètre la valeur en int de valeurDe
     * @param valeurDé
     */
    public void setValeurDé(int valeurDé) {
        this.valeurDé = valeurDé;
    }
}
