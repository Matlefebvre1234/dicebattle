package com.example.dicebattle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Objet potion redonne 5 de vie
 */
public class Potion extends Objet {
    Joueur joueur;

    /**
     * Ceci est le constructeur de base de la classe Potion
     * @param player
     */
    Potion(Joueur player) {
        super("Potion de vie", " +5 points de vie", R.drawable.petitepotion,2);

    joueur = player;

    }

    /**
     * ceci est une méthode qui donne un effet a la vie du joueur
     * @return
     */
    @Override
    /**
     * La fonction effetObjet enleve de la mana au joueur et ajoute de la vie
     */
    public Joueur effetObjet() {

            joueur.setVie(joueur.getVie() + 5);
            joueur.setMana(joueur.getMana() - 2);

        return joueur;
    }

    @Override
    public void ConsummerMana() {

    }
}
