package com.example.dicebattle;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Joueur ayant un nom ,des points de vie et de la mana
 */
public class Joueur implements Parcelable {

    private int vie;
    private String nom;
    private int mana;
    private int tour;

    /**
     *Constructeur de la classe Joueur
     * @param vie1 ceci est la vie du joueur
     * @param nom1 ceci est le nom du joueur
     * @param mana1 ceci est le nombre de mana du joueur
     * @param tour1 ceci est le nombre de tour
     *  ceci est le constructeur de la classe joueur
     */
    Joueur(int vie1 ,String nom1,int mana1, int tour1)
    {
        setVie(vie1);
        setNom(nom1);
        setMana(mana1);
        tour = tour1;
    }


    protected Joueur(Parcel in) {
        vie = in.readInt();
        nom = in.readString();
        mana = in.readInt();
    }

    public static final Creator<Joueur> CREATOR = new Creator<Joueur>() {
        @Override
        public Joueur createFromParcel(Parcel in) {
            return new Joueur(in);
        }

        @Override
        public Joueur[] newArray(int size) {
            return new Joueur[size];
        }
    };

    /**
     * ceci est le getter de la vie
     * @return
     */
    public int getVie() {
        return vie;
    }

    /**
     * ceci est le setter de la vie
     * @param vie
     */
    public void setVie(int vie) {
        this.vie = vie;
    }

    /**
     * ceci est le getter du nom de la classe Joueur
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     * ceci est le setter du nom de la classe Joueur
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * ceci est le getter de la mana dans la classe Joueur
     * @return
     */
    public int getMana() {
        return mana;
    }

    /**
     * ceci est le setter de la mana dans la classe Joueur
     * @param mana
     */
    public void setMana(int mana) {
        this.mana = mana;
    }


    /**
     * ceci est le getter du nombre de tour dans la classe Joueur
     * @return
     */
    public int getTour() {
        return tour;
    }

    /**
     * ceci est le setter du nombre de tour dans la classe Joueur
     * @param tour
     */
    public void setTour(int tour) {
        this.tour = tour;
    }


    @Override
    /**
     *
     */
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(vie);
        dest.writeString(nom);
        dest.writeInt(mana);
    }
}
