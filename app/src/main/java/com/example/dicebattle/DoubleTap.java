package com.example.dicebattle;

/**
 * Objet DoubleTap Double les dégats
 */
public class DoubleTap extends Objet {

    private Joueur joueur;



    /**
     *
     * @param joueur1 joueur1 est le  joueur qui utilise l'objet
     * Ceci est le constructeur de la classe DoubleTap
     */
    DoubleTap(Joueur joueur1) {
        super("DoubleTap","Dégât X2",R.drawable.doubletap, 3);


        joueur = joueur1;

    }

    @Override
    /**
     * Ceci applique l'effet de l'objet
     */
    public Joueur effetObjet() {

        joueur.setMana(joueur.getMana() - 3);
        return joueur;
    }
}
