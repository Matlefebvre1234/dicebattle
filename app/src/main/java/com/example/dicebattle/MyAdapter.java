package com.example.dicebattle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * adapter du recycler view
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    ArrayList<Objet> list = new ArrayList<>();
    private OnNoteClickListener mOnNoteListener;


    MyAdapter(ArrayList<Objet> maList,OnNoteClickListener onNoteListener)
    {
        list = maList;
        this.mOnNoteListener = onNoteListener;

    }
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.objetrow,parent,false);
        return new MyViewHolder(view,mOnNoteListener);
    }

    @Override
    public void onBindViewHolder(MyAdapter.MyViewHolder holder, int position) {

        holder.display(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nomObjet;
        TextView descriptionObjet;
        TextView manaObjet;
        ImageView image;
        OnNoteClickListener mOnCliclListener;

        MyViewHolder(View itemView,OnNoteClickListener listener)
        {
            super(itemView);
            nomObjet = (TextView) itemView.findViewById(R.id.textViewnomObjet);
            descriptionObjet = (TextView) itemView.findViewById(R.id.textViewDescriptionObjet);
            image = (ImageView)itemView.findViewById(R.id.imageViewObjet);
            manaObjet = itemView.findViewById(R.id.textViewMana);
            this.mOnCliclListener = listener;

            itemView.setOnClickListener(this);
        }


        void display(Objet objet)
        {

            nomObjet.setText(objet.getNomObjet());
            descriptionObjet.setText(String.valueOf(objet.getDescription()));
            manaObjet.setText("Mana: "+String.valueOf(objet.getMana()));
            image.setImageResource(objet.getImageObjet());


        }

        @Override
        public void onClick(View view) {
            mOnCliclListener.OnNoteClick(getAdapterPosition());
        }
    }

    public interface OnNoteClickListener{

        void OnNoteClick(int position);
    }


}
