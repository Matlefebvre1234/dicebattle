package com.example.dicebattle;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * Page principal de l'application
 */
public class MainActivity extends AppCompatActivity {

    public static final Random RANDOM = new Random();
    /**
     *
     */

    private MediaPlayer player;
    private Button bouttonlancerDesJoueur1;
    private Button bouttonObjetJoueur1;
   // private Button BouttonDesSpeciauxJoueur1;

    private Button bouttonlancerDesJoueur2;
    private Button bouttonObjetJoueur2;
  //  private Button BouttonDesSpeciauxJoueur2;

    private boolean tourJoueur1;
    private boolean tourJoueur2 ;
    private boolean gameOver;
    private boolean pauseMusic = true;
    private Toast toast;
    private Toast toastmana;

    Joueur joueur1;
    Joueur joueur2;



    private Dé déJoueur;
    private Dé déJoueur2;
    private TextView viewVieJoueur1;
    private TextView viewVieJoueur2;
    private TextView textViewJoueur1;
    private TextView textViewJoueur2;
    private TextView textViewManaJ1;
    private TextView textViewManaJ2;

    private int vieTotal  = 40;



    private String nomjoueur1;
    private String nomjoueur2;

    private ImageView imageDesJoueur, imageDesEnnemi;

    @Override
    /**
     * ceci est le constructeur du MainActivity
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        play(R.raw.yugioh);
        player.setLooping(true);


       final Bundle intent = getIntent().getExtras();

        nomjoueur1 = intent.getString("nomJoueur1","a");
        nomjoueur2 = intent.getString("nomJoueur2","b");

        joueur1 = new Joueur(vieTotal,nomjoueur1,0,0);
        joueur2 = new Joueur(vieTotal,nomjoueur2,0,0);


        gameOver = false;

        tourJoueur1 = true;
        tourJoueur2 = false;



        imageDesJoueur = (ImageView) findViewById(R.id.ImageDesJoueur);
        imageDesEnnemi = (ImageView) findViewById(R.id.ImageDesEnnemi);



        viewVieJoueur1 = (TextView) findViewById(R.id.textViewVieJoueur);
        viewVieJoueur2 = (TextView) findViewById(R.id.textViewVieEnnemi);
        textViewManaJ1 = findViewById(R.id.textViewManaJ1);
        textViewManaJ2 = findViewById(R.id.textViewManaJ2);




       textViewJoueur1 = (TextView) findViewById(R.id.nomJoueur1Main);
        textViewJoueur2 = (TextView) findViewById(R.id.nomJoueur2Main);

        textViewJoueur1.setText(nomjoueur1);
        textViewJoueur2.setText(nomjoueur2);

        déJoueur = new Dé(1);
        déJoueur2 = new Dé(1);




        bouttonlancerDesJoueur1 = (Button) findViewById(R.id.RollButtonJoueu1);
        bouttonObjetJoueur1 = (Button) findViewById(R.id.ButtonObjetJoueur1);


        bouttonlancerDesJoueur2 = (Button) findViewById(R.id.RollButtonJoueu2);
        bouttonObjetJoueur2 = (Button) findViewById(R.id.boutonObjetJoueur2);


        bouttonObjetJoueur2.setEnabled(false);
        bouttonlancerDesJoueur2.setEnabled(false);

        gameStart();
/**
 * ceci est le clicklistener du buton lancerdé du joueur 1
 */
        bouttonlancerDesJoueur1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int valeurDesJoueur = déJoueur.randomDiceValue();

               changerImageDé(valeurDesJoueur);

                DamageToJoueur2(valeurDesJoueur);
                tourJouer();

            }
        });
/**
 * ceci  est le bouton qui ouvre la page des objets pour le joueur 1
 */
        bouttonObjetJoueur1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intentObjet = new Intent(MainActivity.this,PageObjetJoueur1.class);
                intentObjet.putExtra("joueur",joueur1);
                intentObjet.putExtra("rotation",0);
                pauseMusic = false;
                startActivityForResult(intentObjet,1);
                if(toast!= null)toast.cancel();
                if(toastmana!= null)toast.cancel();


            }
        });

/**
 * ceci  est le bouton qui ouvre la page des objets pour le joueur 2
 */
        bouttonObjetJoueur2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intentObjet = new Intent(MainActivity.this,PageObjetJoueur1.class);
                intentObjet.putExtra("joueur",joueur2);
                intentObjet.putExtra("rotation",180);
                pauseMusic = false;
                startActivityForResult(intentObjet,2);
                if(toast!= null)toast.cancel();
                if(toastmana!= null)toast.cancel();

            }
        });

/**
 * ceci est le clicklistener du buton lancerdé du joueur 2
 */
        bouttonlancerDesJoueur2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int valeurDesEnnemi = déJoueur2.randomDiceValue();

                changerImageDé(valeurDesEnnemi);
                DamageToJoueur1(valeurDesEnnemi);
                tourJouer();

            }
        });
    }


    /**
     * ceci est la fonction qui effectue le changement de tour
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void changerTour()
    {

        if(tourJoueur1 == true)
        {
            bouttonlancerDesJoueur1.setEnabled(true);
            bouttonlancerDesJoueur1.setBackground(getResources().getDrawable(R.drawable.boutoncool));
            bouttonlancerDesJoueur1.setTextColor(getResources().getColor(R.color.bleu));
            bouttonObjetJoueur1.setEnabled(true);
            bouttonObjetJoueur1.setBackground(getResources().getDrawable(R.drawable.boutoncool));
            bouttonObjetJoueur1.setTextColor(getResources().getColor(R.color.bleu));

            bouttonlancerDesJoueur2.setEnabled(false);
            bouttonlancerDesJoueur2.setBackground(getResources().getDrawable(R.drawable.boutoncoolnoir));
            bouttonlancerDesJoueur2.setTextColor(getResources().getColor(R.color.noir));
            bouttonObjetJoueur2.setEnabled(false);
            bouttonObjetJoueur2.setBackground(getResources().getDrawable(R.drawable.boutoncoolnoir));
            bouttonObjetJoueur2.setTextColor(getResources().getColor(R.color.noir));

        }
        else {
            bouttonlancerDesJoueur1.setEnabled(false);
            bouttonlancerDesJoueur1.setBackground(getResources().getDrawable(R.drawable.boutoncoolnoir));
            bouttonlancerDesJoueur1.setTextColor(getResources().getColor(R.color.noir));
            bouttonObjetJoueur1.setEnabled(false);
            bouttonObjetJoueur1.setBackground(getResources().getDrawable(R.drawable.boutoncoolnoir));
            bouttonObjetJoueur1.setTextColor(getResources().getColor(R.color.noir));

            bouttonlancerDesJoueur2.setEnabled(true);
            bouttonlancerDesJoueur2.setBackground(getResources().getDrawable(R.drawable.boutoncoolrouge));
            bouttonlancerDesJoueur2.setTextColor(getResources().getColor(R.color.rouge));
            bouttonObjetJoueur2.setEnabled(true);
            bouttonObjetJoueur2.setBackground(getResources().getDrawable(R.drawable.boutoncoolrouge));
            bouttonObjetJoueur2.setTextColor(getResources().getColor(R.color.rouge));

        }



    }

    /**
     * Ceci est la fonction qui incrémente et affiche la mana lorsque le tour du joueur finit
     */
    public void tourJouer()
    {

        if(tourJoueur1 == true)
        {
            tourJoueur1 = false;
            tourJoueur2 = true;


            joueur1.setTour(1);
            joueur2.setTour(1);

            if(joueur2.getMana() < 10)
            {
                joueur2.setMana(joueur2.getMana() + 1);
                textViewManaJ2.setText( String.valueOf(joueur2.getMana()) + "/10");

                joueur2.setTour(joueur2.getTour() +1);


            }
        }
        else {

            tourJoueur2 = false;
            tourJoueur1 = true;
            if(joueur1.getMana() < 10)
            {
                joueur1.setMana(joueur1.getMana() + 1);
                textViewManaJ1.setText( String.valueOf(joueur1.getMana()) + "/10");



                joueur1.setTour(joueur1.getTour() +1);

            }


        }


        changerTour();
        if(gameOver == false) toastPlusMana(1);

    }

    /**
     * Ceci est la fonction qui fait des dégats au joueur2 et si la vie de celui-ci est plus petit que 0 le joueur1 a perdu. Il y a aussi un toast qui s'affiche pour montrer les degats
     * @param dommage
     */
    public void DamageToJoueur1(int dommage)
    {

        joueur1.setVie(joueur1.getVie() - dommage);
        viewVieJoueur1.setText(String.valueOf(joueur1.getVie()) +"/"+String.valueOf(vieTotal));
        if(joueur1.getVie() <= 0)
        {
            joueur1.setVie(0);
            gameOver(joueur2);

        }

        if(gameOver == false) toastDamage(dommage);
    }


    /**
     * Ceci est la fonction qui fait des dégats au joueur2 et si la vie de celui-ci est plus petit que 0 le joueur2 a perdu. Il y a aussi un toast qui s'affiche pour montrer les degats
     * @param dommage
     */
    public void DamageToJoueur2(int dommage)
    {

        joueur2.setVie(joueur2.getVie() - dommage);
        viewVieJoueur2.setText(String.valueOf(joueur2.getVie()) +"/"+String.valueOf(vieTotal));
        if(joueur2.getVie() <= 0)
        {
            joueur2.setVie(0);
            gameOver(joueur1);

        }
         if(gameOver == false)toastDamage(dommage);

    }


    /**
     * Ceci est le toast affiché dans les fonctions DamageToJoueur1 et DamageToJoueur2 pour afficher les degats
     * @param damage
     */
    public void toastDamage(int damage)
    {
        if(toast!= null)toast.cancel();
        LayoutInflater in = getLayoutInflater();
        View toastView = in.inflate(R.layout.damagetoast,null);
        TextView textviewJoueur1 = (TextView)toastView.findViewById(R.id.textViewDamage);

        if(tourJoueur1 == true)
        {
            textviewJoueur1.setRotation(0);
            textviewJoueur1.setTextColor(getResources().getColor(R.color.rouge));
        }
        else
        {
            textviewJoueur1.setRotation(180);
            textviewJoueur1.setTextColor(getResources().getColor(R.color.bleu));
        }
        textviewJoueur1.setText("-"+String.valueOf(damage));


         toast = new Toast(getApplicationContext());


       LayoutInflater inflater = getLayoutInflater();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;



        if(tourJoueur1)toast.setGravity(Gravity.RIGHT,width/5,0);
        else toast.setGravity(Gravity.LEFT,width/5,0);

        toast.setView(toastView);
       toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();

    }


    /**
     * Ceci est le toast qui est affiché lorqu'un joueur utilise la potion ou la grosse potion et il indique les points de vie recupéré
     * @param damage
     */
    public void toastSoin(int damage)
    {
        if(toast!= null)toast.cancel();
        LayoutInflater in = getLayoutInflater();
        View toastView = in.inflate(R.layout.damagetoast,null);
        TextView textviewJoueur1 = (TextView)toastView.findViewById(R.id.textViewDamage);


        textviewJoueur1.setText("+"+String.valueOf(damage));
        textviewJoueur1.setTextColor(getResources().getColor(R.color.vert));
        if(tourJoueur2) textviewJoueur1.setRotation(180);
        else textviewJoueur1.setRotation(0);


        toast = new Toast(getApplicationContext());


        LayoutInflater inflater = getLayoutInflater();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;



        if(tourJoueur1)toast.setGravity(Gravity.RIGHT,width/5,0);
        else toast.setGravity(Gravity.LEFT,width/5,0);

        toast.setView(toastView);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();

    }


    /**
     * Toast afficher losque qu'un joueur perd de la mana
     * @param mana nombre de mana enlevé
     */
    public void toastMoinsMana(int mana)
    {
        if(toast!= null)toast.cancel();
        LayoutInflater in = getLayoutInflater();
        View toastView = in.inflate(R.layout.damagetoast,null);
        TextView textviewJoueur1 = (TextView)toastView.findViewById(R.id.textViewDamage);


        textviewJoueur1.setText("-"+String.valueOf(mana));
        textviewJoueur1.setTextColor(getResources().getColor(R.color.mauve));
        if(tourJoueur2) textviewJoueur1.setRotation(180);
        else textviewJoueur1.setRotation(0);


        toast = new Toast(getApplicationContext());


        LayoutInflater inflater = getLayoutInflater();


        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        int width = size.x;



        if(tourJoueur1)toast.setGravity(Gravity.RIGHT,width/5,0);
        else toast.setGravity(Gravity.LEFT,width/5,0);

        toast.setView(toastView);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();




    }

    /**
     * toast losqu'un joueur gagne de la mana
     * @param mana mana gagné
     */
    public void toastPlusMana(int mana)
    {
        if(toastmana!= null)toastmana.cancel();
        LayoutInflater in = getLayoutInflater();
        View toastView = in.inflate(R.layout.damagetoast,null);
        TextView textviewJoueur1 = (TextView)toastView.findViewById(R.id.textViewDamage);


        textviewJoueur1.setText("+"+String.valueOf(mana));
        textviewJoueur1.setTextColor(getResources().getColor(R.color.mauve));
        if(tourJoueur2) textviewJoueur1.setRotation(180);
        else textviewJoueur1.setRotation(0);


        toastmana = new Toast(getApplicationContext());


        LayoutInflater inflater = getLayoutInflater();


        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        int width = size.x;



        if(tourJoueur1)toastmana.setGravity(Gravity.RIGHT,width/5,0);
        else toastmana.setGravity(Gravity.LEFT,width/5,0);

        toastmana.setView(toastView);
        toastmana.setDuration(Toast.LENGTH_SHORT);
        toastmana.show();




    }



    /**
     * Ceci est la fonction gameOver qui est appelé lorsqu'un joueur  a moin de 0pv dans DamageToJoueur1 et DamageToJoueur2
     * @param nomJoueur
     */
    public void gameOver(Joueur nomJoueur)
    {
        gameOver = true;
        if(toast!= null)toast.cancel();
        if(toastmana!= null)toast.cancel();
        Intent intent = new Intent(this,PageGameOver.class);
        intent.putExtra("NomJoueurGagnant",nomJoueur);
        intent.putExtra("nomJoueur1",joueur1.getNom());
        intent.putExtra("nomJoueur2",joueur2.getNom());
        intent.putExtra("vieJoueur1",joueur1.getVie());
        intent.putExtra("vieJoueur2",joueur2.getVie());
        startActivity(intent);

    }

    /**
     * Gère les données recu par la page d'objet et actualise le jeu
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        String objetutilisé;

        super.onActivityResult(requestCode, resultCode, data);

        pauseMusic = true;

        //joueur1
        if(requestCode ==1 )
        {
            if(resultCode== RESULT_OK)
            {

                joueur1 = (Joueur) data.getParcelableExtra("joueurObjet1");

                objetutilisé = data.getStringExtra("ObjetUtilisé");




                if(objetutilisé.contains("potion"))
                {

                    potion(joueur1);
                    viewVieJoueur1.setText(String.valueOf(joueur1.getVie()) + "/"+vieTotal);
                    textViewManaJ1.setText( joueur1.getMana() + "/10");
                    tourJouer();

                }

                if (objetutilisé.contains("DoubleTap"))
                {
                    int valeur = déJoueur2.randomDiceValue();

                    DamageToJoueur2(valeur * 2);
                    changerImageDé(valeur);
                    textViewManaJ1.setText( joueur1.getMana() + "/10");
                    tourJouer();

                }



                if (objetutilisé.contains("grossePopo"))
                {
                    grossepotion(joueur1);
                    viewVieJoueur1.setText(String.valueOf(joueur1.getVie()) + "/"+vieTotal);
                    textViewManaJ1.setText( joueur1.getMana() + "/10");
                    tourJouer();

                }


                if (objetutilisé.contains("TripleTap"))
                {
                    int valeur = déJoueur.randomDiceValue();

                    DamageToJoueur2(valeur * 3);
                    changerImageDé(valeur);
                    textViewManaJ1.setText( joueur1.getMana() + "/10");
                    tourJouer();

                }

                if (objetutilisé.contains("Parasite"))
                {

                    joueur2.setMana(joueur2.getMana() - 3);
                    Mana(joueur2);
                    textViewManaJ2.setText( joueur2.getMana() + "/10");
                    tourJouer();

                }




            }


        }

        //joueur 2
        if(requestCode == 2 )
        {
            if(resultCode== RESULT_OK)
            {

                joueur2 = (Joueur) data.getParcelableExtra("joueurObjet1");

                objetutilisé = data.getStringExtra("ObjetUtilisé");

                if(objetutilisé.contains("potion"))
                {
                    potion(joueur2);
                    viewVieJoueur2.setText(String.valueOf(joueur2.getVie()) + "/"+vieTotal);
                    textViewManaJ2.setText( joueur2.getMana() + "/10");
                    tourJouer();
                }

                if (objetutilisé.contains("DoubleTap"))
                {
                    int valeur = déJoueur.randomDiceValue();

                    DamageToJoueur1(valeur * 2);
                    changerImageDé(valeur);
                    textViewManaJ2.setText( joueur2.getMana() + "/10");
                    tourJouer();

                }

                if (objetutilisé.contains("grossePopo"))
                {
                    grossepotion(joueur2);
                    viewVieJoueur2.setText(String.valueOf(joueur2.getVie()) + "/"+vieTotal);
                    textViewManaJ2.setText( joueur2.getMana() + "/10");
                    tourJouer();

                }

                if (objetutilisé.contains("TripleTap"))
                {
                    int valeur = déJoueur.randomDiceValue();

                    DamageToJoueur1(valeur * 3);
                    changerImageDé(valeur);
                    textViewManaJ2.setText( joueur2.getMana() + "/10");
                    tourJouer();

                }


                if (objetutilisé.contains("Parasite"))
                {

                    joueur1.setMana(joueur1.getMana() - 3);
                    Mana(joueur1);
                    textViewManaJ2.setText( joueur2.getMana() + "/10");
                    tourJouer();

                }


            }


        }

    }

    /**
     * Ceci est la fonction potion qui  guerit le joueur de 5pv et affiche le toast de soin
     * @param joueur
     */
    public void potion(Joueur joueur)
    {

        if (joueur.getVie() > 40)
        {
            int totalSoin;

            totalSoin = 45 - joueur.getVie();
            joueur.setVie(40);
            toastSoin(totalSoin);
        }
        else toastSoin(5);

    }


    /**
     * Détermine le joueur qui commence à jouer
     */
    public void gameStart(){

        int val = déJoueur.randomDiceValue();

        if(val == 1 || val == 3 || val == 5)
        {
            tourJoueur1 = true;
            tourJoueur2 = false;
            changerTour();


        }

        else {
            tourJoueur2 = true;
            tourJoueur1 = false;
            changerTour();

        }



    }


    /**
     * Calcul la mana à enlever a un joueur et affiche le toast
     * @param joueur
     */
    public void Mana(Joueur joueur)
    {

        if (joueur.getMana() < 0)
        {
            int totalMana;

            totalMana = 3 + joueur.getMana();
            joueur.setMana(0);
            toastMoinsMana(totalMana);
        }
        else toastMoinsMana(3);

    }


    /**
     * Ceci est la fonction grossePotion qui  guerit le joueur de 8pv et affiche le toast de soin
     * @param joueur
     */
    public void grossepotion(Joueur joueur)
    {

        if (joueur.getVie() > 40)
        {
            int totalSoin;

            totalSoin = 48 - joueur.getVie();
            joueur.setVie(40);
            toastSoin(totalSoin);
        }
        else toastSoin(8);

    }

    /**
     * La fonction changerImageDe change l image selon la valeur du dé
     * @param valeurdé
     */
    public void changerImageDé(int valeurdé)
    {
        if(tourJoueur1)
        switch (valeurdé){

            case 1:
                imageDesJoueur.setImageResource(R.drawable.d1);

                break;
            case 2:
                imageDesJoueur.setImageResource(R.drawable.d2);

                break;
            case 3:
                imageDesJoueur.setImageResource(R.drawable.d3);

                break;
            case 4:
                imageDesJoueur.setImageResource(R.drawable.d4);

                break;
            case 5:
                imageDesJoueur.setImageResource(R.drawable.d5);

                break;
            case 6:
                imageDesJoueur.setImageResource(R.drawable.d6);
                break;


        }else { switch (valeurdé){

            case 1:
                imageDesEnnemi.setImageResource(R.drawable.d1);

                break;
            case 2:
                imageDesEnnemi.setImageResource(R.drawable.d2);

                break;
            case 3:
                imageDesEnnemi.setImageResource(R.drawable.d3);

                break;
            case 4:
                imageDesEnnemi.setImageResource(R.drawable.d4);

                break;
            case 5:
                imageDesEnnemi.setImageResource(R.drawable.d5);

                break;
            case 6:
                imageDesEnnemi.setImageResource(R.drawable.d6);

                break;
        }




        }



    }

    //music

    /**
     * met  play la music
     * @param music
     */
    public void play(int music) {
        if (player == null) {
            player = MediaPlayer.create(this,music);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlayer();
                }
            });
        }

        player.start();
    }

    /**
     * met pause la music
     */
    public void pause() {
        if (player != null) {
            player.pause();
        }
    }

    /**
     * delete le mediaPlayer
     */
    public void stop() {
        stopPlayer();
    }

    private void stopPlayer() {
        if (player != null) {
            player.release();
            player = null;

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
      if(pauseMusic == true)  stopPlayer();

}


}
