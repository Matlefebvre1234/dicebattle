package com.example.dicebattle;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Page Accueil de l'application
 */
public class PageAccueil extends AppCompatActivity {

    private Button boutonContinuer;
    private TextView nomJoueur1;
    public static SQLiteDatabase mydb;
    private TextView nomJoueur2;
    private TextView txtDiceBattle;
    private   MediaPlayer player;
    private   MediaPlayer player2;


    /**
     * Constructeur de la Page Accueil
     * @param savedInstanceState
     */
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pageaccueil);

        //musique
        play();
        play2(R.raw.choosechara);

        //base de données
        mydb = openOrCreateDatabase("myDataBase", MODE_PRIVATE, null);
        mydb.execSQL("CREATE TABLE IF NOT EXISTS Joueur(nom VARCHAR,victoire INT);");
        nomJoueur1 = (TextView)findViewById(R.id.PageAccueilNomJoueur1);
        nomJoueur2 = (TextView)findViewById(R.id.PageAccueilNomJoueur2);

        txtDiceBattle = findViewById(R.id.textViewDiceBatlle);
        txtDiceBattle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        txtDiceBattle.setSelected(true);


        boutonContinuer = (Button) findViewById(R.id.buttonAccueil);


/**
 * Click listener du bouton continuer ,Gere les intents et les nom entrés dans la base de données
 */
        boutonContinuer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String nomJ1 =  nomJoueur1.getText().toString();
                String nomJ2 = nomJoueur2.getText().toString();

                Cursor resultSet;

                resultSet = mydb.rawQuery("Select * from Joueur WHERE nom = ?", new String[]{nomJ1});
                if(resultSet.getCount() == 0)
                {
                    mydb.execSQL("INSERT INTO Joueur (nom,victoire) VALUES('"+nomJ1+"',0)");
                }
                resultSet = mydb.rawQuery("Select * from Joueur WHERE nom = ?", new String[]{nomJ2});

                if(resultSet.getCount() == 0)
                {
                    mydb.execSQL("INSERT INTO Joueur (nom,victoire) VALUES('"+nomJ2+"',0)");

                }


                if(!nomJ1.isEmpty() & !nomJ2.isEmpty())
                {   stop2();
                    stop();
                    play2(R.raw.audience);
                    Intent intent = new Intent(PageAccueil.this,MainActivity.class);
                    intent.putExtra("nomJoueur1", nomJ1);
                    intent.putExtra("nomJoueur2",nomJ2);

                    startActivity(intent);
                    finish();}
                else Toast.makeText(PageAccueil.this, "Noms invalides", Toast.LENGTH_SHORT).show();

            }

        });
    }

    //musique
    public void play() {
        if (player == null) {
            player = MediaPlayer.create(this, R.raw.smashtrap);
            player.setLooping(true);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlayer();
                }
            });
        }

        player.start();
    }

    public void pause() {
        if (player != null) {
            player.pause();
        }
    }

    public void stop() {
        stopPlayer();
    }

    private void stopPlayer() {
        if (player != null) {
            player.release();
            player = null;

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopPlayer();
    }


    public void play2(int music) {
        if (player2 == null) {
            player2 = MediaPlayer.create(this,music);
            player2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlayer2();
                }
            });
        }

        player2.start();
    }

    public void pause2() {
        if (player2 != null) {
            player2.pause();
        }
    }

    public void stop2() {
        stopPlayer2();
    }

    private void stopPlayer2() {
        if (player2 != null) {
            player2.release();
            player2= null;

        }
    }





}
