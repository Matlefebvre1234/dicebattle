package com.example.dicebattle;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Page ou tous les objets se retrouvent
 */
public class PageObjetJoueur1 extends AppCompatActivity implements MyAdapter.OnNoteClickListener {

    private ArrayList<Objet> listObjet = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private Joueur joueur;
    private boolean potion;
    private Button boutonBack;
    private LinearLayout layout;
    private Intent intentRetour;

    /**
     * Constructeur de Page objet
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pageobjet);
        intentRetour = new Intent(this, MainActivity.class);
        Intent intent = getIntent();
        joueur = intent.getParcelableExtra("joueur");

        mRecyclerView = findViewById(R.id.monRecyclerView);
        layout = findViewById(R.id.layoutPageObjet);
        layout.setRotation(intent.getIntExtra("rotation", 0));
        boutonBack = findViewById(R.id.buttonObjetBack);

        boutonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED, intentRetour);
                finish();
            }
        });

        //listes des objets dans le recycler view
        listObjet.add(new Potion(joueur));
        listObjet.add(new DoubleTap(joueur));
        listObjet.add(new GrossePotion(joueur));
        listObjet.add(new TripleTap(joueur));
        listObjet.add(new Parasite(joueur));


        //adapter
        mAdapter = new MyAdapter(listObjet, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);


    }

    /**
     * Click listener du recycler view
     * @param position position de l'objet clicker
     */
    @Override
    public void OnNoteClick(int position) {

        //detecter quel objet a été clicker
        //potion
        if (position == 0) {
            if (joueur.getMana() >= 2) {
                joueur = listObjet.get(position).effetObjet();
                intentRetour.putExtra("ObjetUtilisé", "potion");

                intentRetour.putExtra("joueurObjet1", joueur);

                setResult(RESULT_OK, intentRetour);
                finish();
            } else Toast.makeText(this, "Mana insuffisante", Toast.LENGTH_SHORT).show();

        }
        //Doubletap
        if (position == 1) {
            if (joueur.getMana() >= 3) {
                joueur = listObjet.get(position).effetObjet();
                intentRetour.putExtra("ObjetUtilisé", "DoubleTap");

                intentRetour.putExtra("joueurObjet1", joueur);

                setResult(RESULT_OK, intentRetour);
                finish();
            } else Toast.makeText(this, "Mana insuffisante", Toast.LENGTH_SHORT).show();

        }
//        GrossePotion
        if (position == 2) {
            if (joueur.getMana() >= 3) {
                joueur = listObjet.get(position).effetObjet();
                intentRetour.putExtra("ObjetUtilisé", "grossePopo");

                intentRetour.putExtra("joueurObjet1", joueur);

                setResult(RESULT_OK, intentRetour);
                finish();
            } else Toast.makeText(this, "Mana insuffisante", Toast.LENGTH_SHORT).show();

        }
        //Tripletap
        if (position == 3) {
            if (joueur.getMana() >= 5) {

                joueur = listObjet.get(position).effetObjet();
                intentRetour.putExtra("ObjetUtilisé", "TripleTap");

                intentRetour.putExtra("joueurObjet1", joueur);

                setResult(RESULT_OK, intentRetour);
                finish();
            } else Toast.makeText(this, "Mana insuffisante", Toast.LENGTH_SHORT).show();

        }

        //Parasite
        if (position == 4) {
            if (joueur.getMana() >= 1) {

                joueur = listObjet.get(position).effetObjet();
                intentRetour.putExtra("ObjetUtilisé", "Parasite");

                intentRetour.putExtra("joueurObjet1", joueur);

                setResult(RESULT_OK, intentRetour);
                finish();
            } else Toast.makeText(this, "Mana insuffisante", Toast.LENGTH_SHORT).show();



        }


    }
}
