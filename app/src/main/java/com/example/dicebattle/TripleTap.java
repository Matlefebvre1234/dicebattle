package com.example.dicebattle;

/**
 * Objet Triple Tap triple les dégat
 */
public class TripleTap extends Objet {

        private Joueur joueur;



        /**
         *
         * @param joueur1 joueur1 est le  joueur qui utilise l'objet
         * Ceci est le constructeur de la classe TripleTap
         */
        TripleTap(Joueur joueur1) {
            super("TripleTap","Dégât X3",R.drawable.tripletap, 5);


            joueur = joueur1;

        }

        @Override
        /**
         * Ceci applique l'effet de l'objet
         */
        public Joueur effetObjet() {

            joueur.setMana(joueur.getMana() - 5);
            return joueur;
        }
    }


