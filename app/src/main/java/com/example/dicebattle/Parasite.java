package com.example.dicebattle;

/**
 * Objet Parasite Enléve 3 de mana
 */
public class Parasite extends Objet {

    private Joueur joueur;

    /**
     * Constructeur de la classe Parasite
     * @param joueur1
     */
    Parasite(Joueur joueur1) {
        super("Parasite", "Enlève 3 manas à l'adversaire", R.drawable.kraken, 1);
        joueur = joueur1;
    }

    /**
     * Effet de parasite
     * @return
     */
    @Override
    public Joueur effetObjet() {

        joueur.setMana(joueur.getMana() - 1);

        return joueur;
    }
}
